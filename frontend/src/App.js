import { useEffect, useState } from 'react';
import './App.css';
import Auth from './Auth';
import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom';
import Home from './Home';
import Header from './Header';

function App() {
  const [user, setUser] = useState(undefined);

  useEffect(() => {
    if(localStorage.getItem('userInfo')) {
      setUser(true)
    }
  }, [user])

  console.log('user', user)

  return (
    <Router>
      <div className="App">
        <Header />
        <Routes>
          {<Route path="/login" element={<Auth type='login'/>} />}
          {<Route path="/signup" element={<Auth type='signup'/>} />}
          <Route path="/" element={user ? <Home /> : <Navigate to="/login"/>} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
