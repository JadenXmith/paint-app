import { useEffect, useState } from 'react';
import { useLocation, useNavigate, Link } from 'react-router-dom';
import './Auth.css';
import axios from 'axios';

const host = 'http://localhost:5000'

function Auth({ type }) {
  const [fullName, setFullName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [signUp, setSignUp] = useState(true);
  const [emptyInput, setEmptyInput] = useState(false);
  const [errorMessage, setErrorMessage] = useState(false);
  const [correctInput, setCorrectInput] = useState(false);
  const [passwordType, setPasswordType] = useState('password');
  const [loading, setLoading] = useState(false);
  const [loadingError, setLoadingError] = useState('');

  const location = useLocation();
  const navigate = useNavigate();

  useEffect(() => {
    type === 'signup' && setSignUp(true);
    type === 'login' && setSignUp(false);
  }, [type, location])

  useEffect(() => {
    let loggedIn = JSON.parse(localStorage.getItem('userInfo'))
    if(loggedIn) {
        navigate('/')
    }
  }, [navigate])

  const handleSubmit = async (e) => {
    e.preventDefault();

    if((signUp && !fullName) || !email || !password) {
      return (
        setEmptyInput(true),
        setErrorMessage(true)
      )
    }

    let signUpUser = {
        fullName, 
        email, 
        password
    }

    let loginUser = {
        email, 
        password
    }

    if(type === 'signup') {
        try{
            setLoading(true)
            const { data } = await axios.post(`${host}/api/register`, signUpUser)
            setLoading(false)
            data.user && window.location.replace('/login');
        } catch (error) {
            if(error && error.response) {
                setLoadingError(error.response.data.message)
            }
            setLoading(false)
        }
        
    } else {
        try{
            setLoading(true)
            const { data } = await axios.post(`${host}/api/login`, loginUser)
            setLoading(false)
            data.user && window.location.replace('/');
            localStorage.setItem('userInfo', JSON.stringify(data.user))
        } catch (error) {
            if(error && error.response) {
                setLoadingError(error.response.data.message)
            }
            setLoading(false)
        }
    }
  }

  const handlePasswordType = () => {
    if(passwordType === 'password') {
        setPasswordType('text')
    } else {
        setPasswordType('password')
    }
  }

  return (
    <div className="auth">

      <div className='form'>
        <form onSubmit={handleSubmit}>
        <h1>{signUp ? 'Sign Up' : 'Login'}</h1>

        {signUp && 
          <div className='formGroup'>
            <label>Full Name</label>
            <input 
              type="text"
              value={fullName}
              onChange={(e) => {
                setFullName(e.target.value)
              }}
              className={`${!fullName && emptyInput && 'emptyInput'} ${fullName.length > 0 && correctInput && 'correctInput'}`}
            />
            {!fullName && errorMessage && <p className='errorMessage'>Full name is required</p> }
          </div>
        }

          <div className='formGroup'>
            <label>Email</label>
            <input 
              type="email" 
              value={email}
              onChange={(e) => {
                setEmail(e.target.value)
                setCorrectInput(true)
              }}
              className={`${!email && emptyInput && 'emptyInput'} ${email.length > 0 && correctInput && 'correctInput'}`}
            />
            {!email && errorMessage && <p className='errorMessage'>Email is required</p> }

          </div>
          <div className='formGroup'>
            <label>Password</label>
            <input 
              type={passwordType}
              value={password}
              onChange={(e) => {
                setPassword(e.target.value)
                setCorrectInput(true)
              }}
              className={`${!password && emptyInput && 'emptyInput'} ${password.length > 0 && correctInput && 'correctInput'}`}
            />
            <div className='togglePassword' onClick={() => handlePasswordType()}>
                {password.length > 0 && (passwordType === 'password' ? 'Show' : 'Hide')}
            </div>
            {!password && errorMessage && <p className='errorMessage'>Password is required</p> }
          </div>

          {loadingError && <p style={{color: 'tomato', fontSize: '.9rem'}}>{loadingError}</p>}
          <div className='btns'>
            <button className='saveBtn' disabled={loading}>
                {signUp ? 
                <>{loading ? 'processing...' : 'Sign Up'}</> : 
                <>{loading ? 'processing...' : 'Login'}</>}
            </button>
          </div>

          <div className='formFooter'>
            {signUp ?
            <p>Already have an account? <Link to="/login">Login</Link></p>
            :
            <p>Don't have an account? <Link to="/signup">Sign Up</Link></p>
            }
          </div>
        </form>
      </div>
    </div>
  );
}

export default Auth;
