import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './header.css';


function Header() {    
    const user = JSON.parse(localStorage.getItem('userInfo'));

    const [userInfo, setUserInfo] = useState(user);

    const logout = () => {
        localStorage.removeItem('userInfo')
        window.location.replace('/login')
        console.log('hello')
    }


  return (
    <div className='header'>
        <h2>
            <Link to="/" className='appName'><i>Paint App</i></Link>
        </h2>
        <div>
            {userInfo ? 
            <>
                <Link to="#">{userInfo.fullName}</Link>
                <Link to="#" onClick={logout}>Logout</Link>
            </>
            :
            <>
                <Link to={{pathname:"/signup"}}>Sign up</Link>
                <Link to="/login">Login</Link>
            </>
            }
        </div>
    </div>
  )
}

export default Header