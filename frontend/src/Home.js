import React from 'react';
import { fabric } from "fabric";
import { FabricJSCanvas, useFabricJSEditor } from "fabricjs-react";
import './App.css';

function Home() {
  const { editor, onReady } = useFabricJSEditor();

  const onAddCircle = () => {
    editor.addCircle();
  };

  const onAddRectangle = () => {
    editor.addRectangle();
  };

  const onAddText = () => {
    editor.addText("Text");
  };

  const onAddArrow = () => {
    drawArrow(editor.canvas, 100, 100, 150, 150);
  };

  const removeObjectFromCanvas = () => {
    editor.canvas.remove(editor.canvas.getActiveObject());
  };
  
  function drawArrow(canvas, fromx, fromy, tox, toy) {
    var angle = Math.atan2(toy - fromy, tox - fromx);

    var headlen = 5; // arrow head size

    // bring the line end back some to account for arrow head.
    tox = tox - headlen * Math.cos(angle);
    toy = toy - headlen * Math.sin(angle);

    // calculate the points.
    var points = [
      {
        x: fromx, // start point
        y: fromy
      },
      {
        x: fromx - (headlen / 4) * Math.cos(angle - Math.PI / 2),
        y: fromy - (headlen / 4) * Math.sin(angle - Math.PI / 2)
      },
      {
        x: tox - (headlen / 4) * Math.cos(angle - Math.PI / 2),
        y: toy - (headlen / 4) * Math.sin(angle - Math.PI / 2)
      },
      {
        x: tox - headlen * Math.cos(angle - Math.PI / 2),
        y: toy - headlen * Math.sin(angle - Math.PI / 2)
      },
      {
        x: tox + headlen * Math.cos(angle), // tip
        y: toy + headlen * Math.sin(angle)
      },
      {
        x: tox - headlen * Math.cos(angle + Math.PI / 2),
        y: toy - headlen * Math.sin(angle + Math.PI / 2)
      },
      {
        x: tox - (headlen / 4) * Math.cos(angle + Math.PI / 2),
        y: toy - (headlen / 4) * Math.sin(angle + Math.PI / 2)
      },
      {
        x: fromx - (headlen / 4) * Math.cos(angle + Math.PI / 2),
        y: fromy - (headlen / 4) * Math.sin(angle + Math.PI / 2)
      },
      {
        x: fromx,
        y: fromy
      }
    ];

    var pline = new fabric.Polyline(points, {
      fill: "white",
      stroke: "black",
      opacity: 1,
      strokeWidth: 2,
      originX: "left",
      originY: "top",
      selectable: true
    });

    canvas.add(pline);

    canvas.renderAll();
  }

  return (
    <div className='home'>
        <div className='drawingBtns'>
          <button onClick={onAddCircle}>Add circle</button>
          <button onClick={onAddRectangle}>Add Rectangle</button>
          <button onClick={onAddArrow}>Add Arrow</button>
          <button onClick={onAddText}>Add Text</button>
          <button onClick={removeObjectFromCanvas}>Remove</button>
        </div>
        <FabricJSCanvas className="sample-canvas" onReady={onReady} />

    </div>
  )
}

export default Home