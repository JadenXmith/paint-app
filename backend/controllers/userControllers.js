const User = require("../models/userModel");
const bcrypt = require('bcryptjs');

const registerUser = async (req, res) => {
    try {
        console.log('body', req.body)
        const { fullName, email, password } = req.body;

        if(!fullName || !email || !password) {
            return res.status(400).json({message: 'Please fill all inputs'})
        }
        
        const user = await User.findOne({email});

        if(user) {
            return res.status(400).json({message: 'User exist already'})
        }

        const hashedPassword = await bcrypt.hash(password, 10);

        const newUser = await User.create({
            fullName,
            email,
            password: hashedPassword
        })

        if(newUser) {
            res.status(201).json({
                message: 'User created successfully',
                user: {
                    _id: newUser._id,
                    fullName: newUser.fullName,
                    email: newUser.email
                }
            })
        } else {
            res.status(500).json({message: 'Sorry, an error occured'})
        }

    } catch (error) {
        console.log('error: ', error)
    }

}

const loginUser = async (req, res) => {
    const { email, password } = req.body;

    if(!email || !password ) {
        return res.status(401).json({message: "Please fill all fields"})
    }

    const user = await User.findOne({email});

    if(!user) {
        return res.status(401).json({message: 'User does not exist'})
    }

    if(user) {
        const comparePassword = await bcrypt.compare(password, user.password)

        if (!comparePassword) {
            return res.status(401).json({message: 'Incorrect email or password'})
        }

        res.status(200).json({
            message: 'User found',
            user: {
                _id: user._id,
                email: user.email,
                fullName: user.fullName
            }
        })
    }
}

module.exports = { registerUser, loginUser }