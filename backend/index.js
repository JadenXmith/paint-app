const express = require('express');
const app = express();
const port = process.env.PORT || 5000;
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');

require('dotenv').config()

mongoose.connect(process.env.dbString, {
    useNewUrlParser: true,
}).then(() => console.log('Database is running successfully'))
    .catch(err => console.log('Error: ', err))

app.use(cors());
app.use(express.json())

app.get('/', (req, res) => {
    res.send('API is running')
})

app.use('/api', userRoutes)

app.listen(port, () => {
    console.log('Server running successfully')
})